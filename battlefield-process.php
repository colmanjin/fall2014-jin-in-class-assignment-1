<!DOCTYPE html>
<html><head><title>Battlefield Process</title></head>
<body>
<?php
	

	if(!(isset($_POST["ammo"])&&isset($_POST["soldiers"])&&isset($_POST["duration"])&&isset($_POST["critique"]))) {
        printf("Variables not Set");
		exit;
	
        
    }
	else{
	
		$mysqli = new mysqli('localhost','colmanjin','password','battlefield');
		if($mysqli->connect_errno) {
			printf("Connection Failed: %s\n", $mysqli->connect_error);
			exit;
		}
		
		$ammo = htmlentities($_POST["ammo"]);
		$soldiers = htmlentities($_POST["soldiers"]);
		$duration = htmlentities($_POST["duration"]);
		$critique = ""+htmlentities($_POST["critique"]);
		
		$ammo+=0;
		$soldiers+=0;
		$duration+=0;
		$critique += "";
		
		$stmt = $mysqli->prepare("insert into reports(ammunition,soldiers,duration, critique) 
			values (?, ?, ?, ?)");
		if(!$stmt) {
			printf("Query Prep Failed: %s\n",$mysqli->error);
			exit;
		}
		$stmt->bind_param('iiis',$ammo,$soldiers, $duration, $critique);
		$stmt->execute();
		$stmt->close();
		
		header("Location: battlefield-submit.html");
		
	}

	
	?>

</body>
</html>