<!DOCTYPE html>
<head>
<title>Battle Analysis</title>
</head>
<body>

	<h1>Battlefield Analysis</h1>

	<h2>Latest Critiques</h2>
<?php 
	$stmt = $mysqli->prepare("select critique from reports order by posted");
	if(!$stmt) {
		printf("Query Prep Failed: %s\n",$mysqli->error);
		exit;
	}
	$stmt->execute();
	$stmt->bind_result($critiques);

	echo '<ul>';
	for($i=0;$i<5;$i++){
		echo '<li>'.$critiques[i].'</li>';
	}
	echo '</ul>';
?>

<h2> Battle Statistics </h2>
<?php
	$stmt = $mysqli->prepare("select ammo,duration,soldiers from reports order by soldiers DESC");
	if(!$stmt) {
		printf("Query Prep Failed: %s\n",$mysqli->error);
		exit;
	}
	$stmt->execute();
	$stmt->bind_result($ammos, $durations, $soldiers);
	
	echo "<table border='1'><tr>	<th>Number of Soldiers</th>	<th>Pounds of Ammunition per Second</th>	</tr>";

	for($i=0; $i<sizeof($ammos); $i++) {
	echo "<tr>";
	echo "<td>" . $soldiers[i] . "</td>";
	echo "<td>" . $ammos[i]/$durations[i] . "</td>";
	echo "</tr>";
	}

	echo "</table>";
?>


<form action="battlefield-submit.html" method="POST">
		<input type="submit" value = "Submit a New Battle Report">
</form>
	
	
</body>
</html>